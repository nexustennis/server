var conn = require("./database");

function getAllTournaments(req, callback) {
	var query = "SELECT * FROM tournaments";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

function getATPtournaments(req, callback) {
	var query = "SELECT * FROM tournaments WHERE tour = 'ATP'";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

function getWTAtournaments(req, callback) {
	var query = "SELECT * FROM tournaments WHERE tour = 'WTA'";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

function getAustralianTournaments(req, callback) {
	var query = "SELECT * FROM tournaments WHERE tour = 'Australian Money Tournament' OR tour = 'Australian Junior Tournament'";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

function getAustralianAMTtournaments(req, callback) {
	var query = "SELECT * FROM tournaments WHERE tour = 'Australian Money Tournament'";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

function getAustralianAJTtournaments(req, callback) {
	var query = "SELECT * FROM tournaments WHERE tour = 'Australian Junior Tournament'";

	conn.query(query, queryTournamentsResult);

	function queryTournamentsResult(err, rows, fields) {
		if (err) throw err;
		req.data.tournaments = rows;
		callback();
	}
}

module.exports = {
	getAllTournaments,
	getATPtournaments,
	getWTAtournaments,
	getAustralianTournaments,
	getAustralianAMTtournaments,
	getAustralianAJTtournaments
}