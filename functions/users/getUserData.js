var conn = require("../database");

function getUserData(req, callback) {
	var query = "SELECT users.*, userData.* FROM users, userData WHERE users.userID = ? AND users.userID = userData.userID";
	query = conn.query(query, [req.session.userID], function (err, rows, fields) {
		if (err) console.log(err);
		req.data.userData = [];
		if (rows && rows.length > 0) req.data.userData = rows[0]; 
		callback();
	});
	console.log(query.sql);
}

module.exports = getUserData;