function filterTournaments(req, callback) {
	filterRadius();

	function filterRadius() {
		if (req.post.locationRange != "radius") {
			finish();
			return;
		}

		var radiusFilteredTournaments = [];
		
		for (var i = 0; i < req.data.unfilteredTournaments.length; i++) {
			console.log(req.data.unfilteredTournaments[i].tournamentName);
			console.log(req.data.unfilteredTournaments[i].latitude + " " + req.data.unfilteredTournaments[i].longitude + " " + req.post.homeCoordinateLatitude + " " + req.post.homeCoordinateLongitude + " " + req.post.radius);
			var distance = getDistanceFromLatLonInKm(req.data.unfilteredTournaments[i].latitude, req.data.unfilteredTournaments[i].longitude, req.post.homeCoordinateLatitude, req.post.homeCoordinateLongitude)
			console.log("Distance: " + distance);
			if (distance < req.post.radius) radiusFilteredTournaments.push(req.data.unfilteredTournaments[i]);
		}

		req.data.unfilteredTournaments = radiusFilteredTournaments;

		finish();
	}

	function finish() {
		req.data.tournaments = req.data.unfilteredTournaments;
		callback();
	}
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

module.exports = filterTournaments;