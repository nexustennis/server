var request = require("../../request");

function getTripTimes(req, callback) {
	url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="; //Vancouver+BC|Seattle

	tournaments = "";

	for (var i = 0; i < req.data.tournaments.length; i++) {
		tournaments += req.data.tournaments[i].latitude;
		tournaments += ",";
		tournaments += req.data.tournaments[i].longitude;
		if (i != req.data.tournaments.length - 1) tournaments += "|";
	}

	url += tournaments;
	url += "&destinations="; //San+Francisco|Victoria+BC
	url += tournaments;
	url += "&key=AIzaSyBP-R7m0I6KkI-DwZUBk62nmDonwpglFng";

	if (req.post.modeOfTransport == "personalCar") fetchPersonalCarTimes();
	else fetchOtherTimes();

	function fetchPersonalCarTimes() {
		request(url + "&mode=driving", function(err, body) {
			req.data.personalCarTimes = JSON.parse(body);
			console.log(req.data.personalCarTimes);
			callback();
		});
	}

	function fetchOtherTimes() {
		if (req.post.otherModesOfTransport.includes("hireCar")) fetchHireCarTimes();
		else if (req.post.otherModesOfTransport.includes("plane")) fetchPlaneTimes();
		else if (req.post.otherModesOfTransport.includes("publicTransport")) fetchPublicTransportTimes();
		else callback();
	}

	function fetchHireCarTimes() {
		request(url + "&mode=driving", function(err, body) {
			req.data.hireCarTimes = JSON.parse(body);
			console.log(req.data.hireCarTimes);
			if      (req.post.otherModesOfTransport.includes("plane"))           fetchPlaneTimes();
			else if (req.post.otherModesOfTransport.includes("publicTransport")) fetchPublicTransportTimes();
			else callback();
		});
	}

	function fetchPlaneTimes() {
		/*request(url + "&mode=transit", function(err, body) {
			req.data.planeTimes = JSON.parse(body);
			console.log(req.data.planeTimes);
			if (req.post.otherModesOfTransport.includes("publicTransport")) fetchPublicTransportTimes();
			else callback();
		});
		*/
		if (req.post.otherModesOfTransport.includes("publicTransport")) fetchPublicTransportTimes();
		else callback();
	}

	function fetchPublicTransportTimes() {
		request(url + "&mode=transit", function(err, body) {
			req.data.hireCarTimes = JSON.parse(body);
			console.log(req.data.hireCarTimes);
			callback();
		});
	}

}

module.exports = getTripTimes;