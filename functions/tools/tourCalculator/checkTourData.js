var conn  = require("../../database");
var isValidDate = require("../../isValidDate");

function checkTourData(req, errors, callback) {
	console.log(req.post);

    if (req.post.startDate == "" || req.post.startDate == undefined) errors.push("Please enter a start date");
    else if (!isValidDate(req.post.startDate)) errors.push("Please enter a valid start date");

    if (req.post.endDate == "" || req.post.endDate == undefined) errors.push("Please enter an end date");
    else if (!isValidDate(req.post.endDate)) errors.push("Please enter a valid end date");

    /*var homeCoordinates = req.post.homeCoordinates.substr(1, req.post.homeCoordinates - 2).split(", ");
    
    if (req.post.homeCoordinate[0] != "" && req.post.homeCoordinates[0] != undefined) errors.push("Please select your home on the map");
    else if (isNaN(req.post.homeCoordinate[0])) errors.push("Please enter a valid latitude");
    else if (req.post.homeCoordinate[1] != "" && req.post.homeCoordinate[1] != undefined) errors.push("Please select your home on the map");
    else if (isNaN(req.post.homeCoordinates[1])) errors.push("Please select your home on the map");
    */

    if (req.post.locationRange == "" || req.post.locationRange == undefined) errors.push("Please select a location range");
    else if (req.post.locationRange != "radius" && req.post.locationRange != "country" && req.post.locationRange != "worldwide")
        errors.push("Invalid location range");
    else if (req.post.locationRange == "radius") {
    	if (req.post.radius != "" && req.post.radius != undefined) errors.push("Please enter a location range radius");
    	else if (isNaN(req.post.radius)) errors.push("Please enter a valid location range radius");
    }

    var otherModesOfTransport = [
        "hireCar",
        "plane",
        "publicTransport"
    ];

    if (req.post.modeOfTransport == "" || req.post.modeOfTransport == undefined) errors.push("Please select a mode of transport");
    else if (req.post.modeOfTransport = "personalCar");
    else if (req.post.modeOfTransport = "other") {
    	if (req.post.otherModesOfTransport != "" && req.post.otherModesOfTransport != undefined) errors.push("Please select an other mode of transport");
    	//else if (req.post.otherModesOfTransport.length == 0) errors.push("Please select one or more other mode of transport");
        else {
            console.log(typeof req.post.otherModesOfTransport);
            if (typeof req.post.otherModesOfTransport == "string") req.post.otherModesOfTransport = [req.post.otherModesOfTransport];
    		for (var i = 0; i < otherModesOfTransport.length; i++) {
    			if (!otherModesOfTransport.includes(req.post.otherModesOfTransport[i])) {
    				errors.push("Invalid other mode of transport");
    				break;
    			}
    		}
    	} 
    } else errors.push("Please enter a valid mode of transport");

    if (req.post.numberOfTournamentsSearchType == "" || req.post.numberOfTournamentsSearchType == undefined) errors.push("Please select an option for number of tournaments");
    else if (req.post.numberOfTournamentsSearchType != "number" && req.post.numberOfTournamentsSearchType != "max") errors.push("Invalid number of tournaments search type");

    if (req.post.travelSearchType == "" || req.post.travelSearchType == undefined) errors.push("Please select a travel search type");
    else if (req.post.travelSearchType != "leastTravelTime" && req.post.travelSearchType != "lowestTravelCost" && req.post.travelSearchType != "shortestTravelDistance")
    	errors.push("Invalid search type");

    var tournamentLevels = [
    		"grandslam",
    		"atpfinals",
    		"atp1000",
    		"atp500",
    		"atp250",
    		"challenger150000",
    		"challenger125000",
    		"challenger100000",
       		"challenger75000",
    		"challenger50000",
    		"futures25000",
    		"futures15000"
    ]

    if (req.post.tournamentLevel == "" || req.post.tournamentLevel == undefined) errors.push("Please select a tournament level option");
    else if (req.post.tournamentLevel != "automatic" && req.post.tournamentLevel != "manual") errors.push("Invalid tournament level option");
    else if (req.post.tournamentLevel == "manual") {
    	if (req.post.manualTournamentLevel == "" || req.post.manualTournamentLevel == undefined) errors.push("Please select a tournament level option");
        else if (typeof req.post.manualTournamentLevel == "string") req.post.manualTournamentLevel = [req.post.manualTournamentLevel];
    	else {
    		for (var i = 0; i < req.post.manualTournamentLevel.length; i++) {
    			if (!tournamentLevels.includes(req.post.manualTournamentLevel[i])) {
    				errors.push("Invalid tournament level");
    				break;
    			}
    		}
    	}
    }

    if (req.post.locationRange == "country") {
        var query = "SELECT countryID FROM countries WHERE name = ?";
        conn.query(query, req.post.country, checkCountryName);
    } else callback();

    function checkCountryName(err, rows, fields) {
        if (!rows.length) errors.push("Invalid country");
        callback();
    }
}

module.exports = checkTourData;