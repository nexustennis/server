var conn = require("../database");

function getUserDataSettings(req, callback) {
    query = "SELECT users.*, userData.* FROM users, userData WHERE users.userID = ? AND users.userID = userData.userID";
    conn.query(query, req.session.userID, userDataResult);

    function userDataResult(err, rows, fields) {
        req.data.userData = rows[0];
        conn.query("SELECT countryID, name FROM countries ORDER BY name ASC;", countriesResult);
    }

    function countriesResult(err, rows, fields) {
        req.data.countries = rows;
        conn.query("SELECT brandID, brandName FROM brands;", brandResult);
    }

    function brandResult(err, rows, fields) {
        req.data.brands = rows;
        conn.query("SELECT brands.brandID, brands.brandName, racquets.racquetID, racquets.racquetName, racquets.model FROM brands, racquets WHERE brands.brandID = racquets.brandID;", racquetResult);
    }

    function racquetResult(err, rows, fields) {
        req.data.racquets = rows;
        queryStrings();
    }

    function queryStrings() {
        conn.query("SELECT strings.*, brands.* FROM strings, brands WHERE strings.brandID = brands.brandID", stringsResult);
    }

    function stringsResult(err, rows, fields) {
        req.data.strings = rows;
        callback();
    }
}

module.exports = getUserDataSettings;