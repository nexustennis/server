var https = require("https");

function botCheck(req, errors, callback) {

    //console.log(req.post);

    if (req.post['g-recaptcha-response'] === undefined || req.post['g-recaptcha-response'] === '' || req.post['g-recaptcha-response'] === null) {
        errors.push("Human verification failed");
        callback();
    } else {
        var secretKey = "6LdNVjkUAAAAAOQlr5SJF9IjSDD2AEMt-VAkJN9C";
        var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" +
                              secretKey + "&response=" + req.post['g-recaptcha-response'] +
                              "&remoteip=" + req.connection.remoteAddress;

        https.get(verificationUrl, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                body = JSON.parse(data);

                if (body.success !== undefined && !body.success) {
                    errors.push("Human verification failed");
                }

                callback();
            });

        }).on("error", (err) => {
            errors.push("Human verification failed");
            callback();
        });
    }
}

module.exports = botCheck;
