var http        = require('http');
var querystring = require('querystring');
var formidable = require('formidable');

function getPost(req, res, callback) {
    var body = "";

    if (typeof callback !== 'function') return null;

    if (req.method == 'POST') {

        req.on('data', function(data) {
            body += data;

            if (body.length > 1e6) {
                body = "";
                res.writeHead(413, {'Content-Type': 'text/plain'}).end();
                req.connection.destroy();
            }
        });

        req.on('end', function() {
            req.post = querystring.parse(body);
            callback();
        });

    } else {
        res.writeHead(405, {'Content-Type': 'text/plain'});
        res.end();
    }
}

function parseBody(req, callback) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        req.post = fields;
        req.files = files;
        callback();
    });
}

module.exports = parseBody;
