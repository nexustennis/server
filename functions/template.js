var ejs  = require('ejs');
var fs   = require('fs');

// Renders HTML templates
function render(res, filepath, data) {

    ejs.renderFile("templates/" + filepath + ".ejs", data, function(err, str) {
        res.write(str);
        res.end();
    });

}

exports.render = render;
