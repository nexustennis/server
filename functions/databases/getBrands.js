var conn = require("../database");

function getBrands(req, callback) {
	var query = "SELECT * FROM brands";
	conn.query(query, queryBrandsResult);

	function queryBrandsResult(err, rows, fields) {
		if (err) throw err;
		req.data.brands = rows;
		callback();
	}
}

module.exports = getBrands;