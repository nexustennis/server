var conn = require("../../database");

function getRacquets(req, callback) {
	var query = "SELECT brands.*, racquets.* FROM racquets, brands WHERE racquets.brandID = brands.brandID";
	console.log(query);
	conn.query(query, queryRacquetsResult);

	function queryRacquetsResult(err, rows, fields) {
		if (err) throw err;
		req.data.racquets = rows;
		callback();
	}
}

module.exports = getRacquets;