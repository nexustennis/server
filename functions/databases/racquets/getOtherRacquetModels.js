var conn = require("../../database");

function getOtherRacquetModels(req, callback) {
	var query = "SELECT brands.*, racquets.* FROM racquets, brands WHERE brands.brandID = ? AND racquets.racquetName = ? AND racquets.model != ?";
	conn.query(query, [req.data.racquet.brandID, req.data.racquet.racquetName, req.data.racquet.model], queryOtherRacquetModelsResult);

	function queryOtherRacquetModelsResult(err, rows, fields) {
		if (err) throw err;
		console.log(rows);
		req.data.otherRacquetModels = rows;
		callback();
	}
}

module.exports = getOtherRacquetModels;