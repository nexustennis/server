var conn = require("../../database");

function getRacquet(req, callback) {
	racquet     = unescape(req.route[2]).split("-");
	brand       = racquet[0];
	racquetName = racquet[1];
	model       = racquet[2];
	
	var query = "SELECT brands.*, racquets.* FROM racquets, brands WHERE brands.brandName = ? AND racquets.racquetName = ? AND racquets.model = ?";
	conn.query(query, [brand, racquetName, model], queryRacquetResult);

	function queryRacquetResult(err, rows, fields) {
		if (err) throw err;
		req.data.racquet = rows;
		callback();
	}
}

module.exports = getRacquet;