var conn = require("../../database");

function getCustomisations(req, callback) {
	var query = "SELECT customisations.* FROM customisations, brands, racquets WHERE customisations.racquetID = racquets.racquetID AND racquets.brandID = brands.brandID AND racquets.racquetID = ?";
	conn.query(query, [req.data.racquet.racquetID], queryCustomisationsResult);

	function queryCustomisationsResult(err, rows, fields) {
		if (err) throw err;
		req.data.racquetCustomisations  = rows;
		callback();
	}
}

module.exports = getCustomisations;