var conn = require("../../database");

function getCustomisations(req, callback) {
	var query = "SELECT customisations.*, brands.*, racquets.* FROM customisations, racquets, brands WHERE customisations.racquetID = racquets.racquetID AND racquets.brandID = brands.brandID";
	conn.query(query, queryCustomisationsResult);

	function queryCustomisationsResult(err, rows, fields) {
		if (err) throw err;
		req.data.customisations = rows;
		callback();
	}
}

module.exports = getCustomisations;