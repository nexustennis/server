var conn = require("../../database");

function getATPsinglesRankings(req, callback) {
	var variables = [];
	var query = "SELECT rankingDateID FROM rankingDates WHERE organisation = 'atp' AND category = 'singles' ";
	if (req.variables.date) {
		query += "AND date = ? ";
		variables.push(req.variables.date);
	}
	query += "ORDER BY date DESC";

	conn.query(query, [variables], rankingDatesResult);

	function rankingDatesResult(err, rows, fields) {
		if (err) throw err;
		if (rows.length > 0) {
			var rankingDateID = rows[0].rankingDateID;
			console.log(rankingDateID);

			var query = "SELECT * FROM rankings WHERE organisation = 'atp' AND category = 'singles' AND rankingDateID = ? ";
			
			if (req.variables.rankRange) {
				query += "";
				variables.push(req.variables.date);
			}

			query += "ORDER BY rank ASC";

			console.log(conn.query(query, [rankingDateID], queryRankingsResult).sql);
		} else {
			req.data.getATPsinglesRankings = [];
			callback();
		}
	}

	function queryRankingsResult(err, rows, fields) {
		if (err) throw err;
		req.data.atpSinglesRankings = rows;
		console.log(rows);
		callback();
	}
}

module.exports = getATPsinglesRankings;