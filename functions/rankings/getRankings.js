var conn = require("../database");

function getRankings(req, callback) {
	var query = "SELECT * FROM rankings";
	conn.query(query, queryRankingsResult);

	function queryRankingsResult(err, rows, fields) {
		if (err) throw err;
		req.data.rankings = rows;
		callback();
	}
}

module.exports = getRankings;