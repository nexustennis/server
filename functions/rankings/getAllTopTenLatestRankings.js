var conn = require("../database");

function getAllTopTenLatestRankings(req, callback) {
	getTopTenLatestATPsinglesRankings();

	function getTopTenLatestATPsinglesRankings() {
		var query = "SELECT rankingDateID FROM rankingDates WHERE organisation = 'atp' AND category = 'singles' ORDER BY date DESC";
		conn.query(query, rankingDatesResult);

		function rankingDatesResult(err, rows, fields) {
			if (err) throw err;
			var rankingDateID = rows[0].rankingDateID;
			console.log(rankingDateID);

			var query = "SELECT * FROM rankings WHERE organisation = 'atp' AND category = 'singles' AND rankingDateID = ? ORDER BY rank ASC  LIMIT 10";
			console.log(conn.query(query, [rankingDateID], queryRankingsResult).sql);
		}

		function queryRankingsResult(err, rows, fields) {
			if (err) throw err;
			req.data.atpTopTenSinglesRankings = rows;
			console.log(rows);
			finish();
		}
	}

	function finish() {
		callback();
	}
}

module.exports = getAllTopTenLatestRankings;