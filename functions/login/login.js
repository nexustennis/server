var genUUID = require("../genUUID");
var conn = require("../database");

function login(req, callback) {
    req.uuid = genUUID();
    var query = "INSERT INTO sessions SET ?";
    var post = {
        uuid: req.uuid,
        contents: JSON.stringify(req.userData)
    }
    conn.query(query, post, callback);
}

module.exports = login;