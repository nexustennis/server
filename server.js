var http         = require('http');
var fs           = require('fs');
var parseCookies = require('./functions/parseCookies');
var conn         = require('./functions/database');
var redirect     = require("./functions/redirect");

var home        = require('./routes/home.js');
var search      = require('./routes/search.js');
var rankings    = require('./routes/rankings.js');
var livescores  = require('./routes/livescores.js');
var tournaments = require('./routes/tournaments.js');
var tools       = require('./routes/tools.js');
var onlinecoach = require('./routes/onlinecoach.js');
var databases   = require('./routes/databases.js');
var login       = require('./routes/login.js');
var logout      = require('./routes/logout.js');
var register    = require('./routes/register.js');
var profile     = require('./routes/profile.js');
var error       = require('./routes/error.js');

//sudo iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 80 -j REDIRECT --to-port 8000

http.createServer(onConnection).listen(8000);

function onConnection(req, res) {
    console.log(req.url);
    
    parseCookies(req);
    req.data = {};

    if (req.cookies.session) {
        conn.query("SELECT * FROM sessions where uuid = ?;", [req.cookies.session], getSession);
    } else {
        req.session = {
            loggedIn: false
        }

        router();
    }

    function getSession(err, rows, fields) {
        if (err) {
            console.log(err);
            redirect(req, "/");
            return;
        }

        if (rows.length == 0) {
            req.session = {
                loggedIn: false
            }
        } else {
            req.session = JSON.parse(rows[0].contents);
        }

        router();
    }

    function router() {
        var temp = req.url.split("?");
        req.route = temp[0].substr(1, req.url.length - 1).split("/");
        req.variables = {};
        if (temp.length == 2) {
            temp = temp[1].split("&");
            for (var i = 0; i < temp.length; i++) {
                var variable = temp[i].split("=");
                req.variables[variable[0]] = variable[1];
            }
        }
        console.log(req.variables);

        if (req.method == "GET") {
            if      (req.route[0] == "")            home.get(req, res);
            //else if (req.route[0] == "search")      search.get(req, res);
            //else if (req.route[0] == "rankings")    rankings.get(req, res);
            //else if (req.route[0] == "livescores")  livescores.get(req, res);
            //else if (req.route[0] == "tournaments") tournaments.get(req, res);
            else if (req.route[0] == "tools")       tools.get(req, res);
            //else if (req.route[0] == "databases")   databases.get(req, res);
            //else if (req.route[0] == "onlinecoach") onlinecoach.get(req, res);
            else if (req.route[0] == "profile")     profile.get(req, res);
            else if (req.route[0] == "login")       login.get(req, res);
            else if (req.route[0] == "register")    register.get(req, res);
            else if (req.route[0] == "logout")      logout.get(req, res);

            else if (req.route[0] == "favicon.ico") fetchFavicon(req, res);
            else fetchResource(req, res);

        } else if (req.method == "POST") {
            if (req.route[0] == "tools")    tools.post(req, res);
            //else if (req.route[0] == "search")   search.post(req, res);
            else if (req.route[0] == "login")    login.post(req, res);
            else if (req.route[0] == "register") register.post(req, res);
            else if (req.route[0] == "profile")  profile.post(req, res);
            else error(req, res, "404");
        } else {
            error(req, res, "404");
        }
    }
}

function fetchFavicon(req, res) {
    fs.readFile("favicon.ico", processFile);

    function processFile(err, data) {
        if (err) {
            console.log(err);
            error(req, res, "404");
        } else {
            res.writeHead(200);
            res.write(data);
            res.end();
        }
    }
}

function fetchResource(req, res) {
    
    fs.readFile("public" + req.url, processFile);

    function processFile(err, data) {
        if (err) {
            error(req, res, "404");
        } else {
            var fileType; // e.g. 'text/plain, css, javascript etc'
            res.writeHead(200/*, {
                'Content-type': 'text/plain'
            }*/);
            res.write(data);
            res.end();
        }
    }
}
