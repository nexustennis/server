var ejs  = require('ejs');
var redirect = require("../functions/redirect");

function error(req, res, err) {

    res.writeHead(err, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/error.ejs", {
        req: req
    }, function(err, str) {
    	if (err) {
    		console.log(err);
    		redirect(res, "/");
    		return;
    	}
    	
        res.write(str);
        res.end();
    });

}

module.exports = error;
