var ejs                 = require('ejs');
var parseCookies        = require('../functions/parseCookies');
var redirect            = require("../functions/redirect");
var conn                = require("../functions/database");
var error               = require('./error.js');
var processPost         = require("../functions/processPost");
var getUserDataSettings = require("../functions/profile/getUserDataSettings");
var fs                  = require('fs');

function get(req, res) {
    if (req.route.length == 1) {
        redirect(res, "/");
        return;
    }

    if (req.session.username == req.route[1]) {
        if      (req.route.length == 2)      myProfile(req, res);
        else if (req.route[2] == "tour")     myTour(req, res);
        else if (req.route[2] == "history")  myPlayingHistory(req, res);
        else if (req.route[2] == "settings") settings(req, res);
        //else if (req.route[2] == "videos")          myVideos(req, res);
        //else if (req.route[2] == "pictures")        myPictures(req, res);
        //else if (req.route[2] == "activity")        myActivity(req, res);
        //else if (req.route[2] == "goals")           myGoals(req, res);
        else error(req, res, "404");
    } else conn.query("SELECT * FROM users where username = ?;", [req.route[1]], checkUser);

    function checkUser(err, rows, fields) {
        if (rows.length == 0) {
            error(req, res, "404");
            return;
        }

        req.data.user = rows[0];
        if      (req.route.length == 2)     userProfile(req, res);
        else if (req.route[2] == "tour")    userTour(req, res);
        else if (req.route[2] == "history") userPlayingHistory(req, res);
        //else if (req.route[2] == "videos")          userVideos(req, res);
        //else if (req.route[2] == "pictures")        userPictures(req, res);
        //else if (req.route[2] == "activity")        userActivity(req, res);
        //else if (req.route[2] == "goals")           userGoals(req, res);
        else error(req, res, "404");
    }
}

function userProfile(req, res) {
    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/profile/userProfile/profile.ejs", {
        req: req,
        data: req.data
    }, sendData);

    function sendData(err, str) {
        if (err) console.error(err);
        res.write(str);
        res.end();
    }
}

function myProfile(req, res) {
    console.log(req.session.userID);
    var query = `
        SELECT    users.*, userData.*, countries.name countryName, rb.brandName racquetBrand, racquets.racquetName racquetName, racquets.model racquetModel, msb.brandName mainStringBrand, ms.stringName mainStringName, ms.guage mainStringGuage, csb.brandName crossStringBrand, cs.stringName crossStringName, cs.guage crossStringGuage 
        FROM      users, userData
        LEFT JOIN countries  ON userData.countryID     = countries.countryID
        LEFT JOIN strings ms ON userData.mainStringID  = ms.stringID 
        LEFT JOIN strings cs ON userData.crossStringID = cs.stringID
        LEFT JOIN racquets   ON userData.racquetID     = racquets.racquetID
        LEFT JOIN brands rb  ON racquets.brandID       = rb.brandID
        LEFT JOIN brands msb ON ms.brandID             = msb.brandID
        LEFT JOIN brands csb ON cs.brandID             = csb.brandID
        WHERE     users.userID    = ?
        AND       userData.userID = users.userID
    `;
    conn.query(query, [req.session.userID], getUserData);

    function getUserData(err, rows, fields) {
        if (err) console.error(err);
        req.data.userData = rows[0];
        req.data.rankings = {};

        renderPage();
        //getInternationalRankings();
    }

    /*function getInternationalRankings() {
        query = "SELECT rankings.*, rankingDates.* FROM rankings, rankingDates WHERE rankings.profileURL = ? AND rankings.rankingDateID = rankingDates.rankingDateID ORDER BY rankingDates.date DESC";
        conn.query(query, [req.data.userData.internationalProfileLink], internationalRankingsResult);
    }

    function internationalRankingsResult(err, rows, fields) {
        if (err) console.error(err);
        console.log(rows);
        req.data.rankings.internationalRankings = rows;

        if (req.data.rankings.internationalRankings.length) 
            req.data.userData.internationalRanking = req.data.rankings.internationalRankings[0].rank;
        else req.data.userData.internationalRanking = null;

        getNationalRankings();
    }

    function getNationalRankings() {
        query = "SELECT rankings.*, rankingDates.* FROM rankings, rankingDates WHERE rankings.profileURL = ? AND rankings.rankingDateID = rankingDates.rankingDateID ORDER BY rankingDates.date DESC";
        conn.query(query, [req.data.userData.nationalProfileLink], nationalRankingsResult);
    }

    function nationalRankingsResult(err, rows, fields) {
        if (err) console.error(err);
        console.log(rows);
        req.data.rankings.nationalRankings = rows;

        if (req.data.rankings.nationalRankings.length) 
            req.data.userData.nationalRanking = req.data.rankings.nationalRankings[0].rank;
        else req.data.userData.nationalRanking = null;

        getITFjuniorRankings();
    }

    function getITFjuniorRankings() {
        query = "SELECT rankings.*, rankingDates.* FROM rankings, rankingDates WHERE rankings.profileURL = ? AND rankings.rankingDateID = rankingDates.rankingDateID ORDER BY rankingDates.date DESC";
        conn.query(query, [req.data.userData.itfJuniorProfileLink], itfJuniorRankingsResult);
    }

    function itfJuniorRankingsResult(err, rows, fields) {
        if (err) console.error(err);
        console.log(rows);
        req.data.rankings.itfJuniorRankings = rows;

        if (req.data.rankings.itfJuniorRankings.length) 
            req.data.userData.itfJuniorRanking = req.data.rankings.itfJuniorRankings[0].rank;
        else req.data.userData.itfJuniorRanking = null;

        if (req.data.userData.racquetID != null) queryRacquetData();
        else if (req.data.userData.countryID != null) queryCountry();
        else renderPage();
    }*/

    function renderPage() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/profile/myProfile/profile.ejs", {
            req: req
        }, sendData);

        function sendData(err, str) {
            if (err) console.error(err);
            res.write(str);
            res.end();
        }
    }
}

function myTour(req, res) {
    if (!req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    var query = "SELECT * FROM tours, tournaments WHERE userID = ? AND tours.tournamentID = tournaments.tournamentID AND startDate > ? ORDER BY startDate ASC";
    conn.query(query, [req.session.userID, (new Date())], renderFile);

    function renderFile(err, rows, fields) {
        req.data.tour = [];
        if (err) console.log(err);
        else {
            req.data.tour = rows;
        }

        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/profile/myProfile/tour.ejs", {
            req: req,
        }, sendData);

        function sendData(err, str) {
            if (err) console.log(err);
            res.write(str);
            res.end();
        }
    }
}

function myPlayingHistory(req, res) {
    if (!req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    var query = "SELECT * FROM tours, tournaments WHERE userID = ? AND tours.tournamentID = tournaments.tournamentID AND startDate < ? ORDER BY startDate DESC";
    conn.query(query, [req.session.userID, (new Date())], renderFile);

    function renderFile(err, rows, fields) {
        req.data.history = [];
        if (err) console.log(err);
        else {
            req.data.history = rows;
        }

        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/profile/myProfile/playingHistory.ejs", {
            req: req,
        }, sendData);

        function sendData(err, str) {
            if (err) console.log(err);
            res.write(str);
            res.end();
        }
    }
}

function settings(req, res) {
    getUserDataSettings(req, renderSettings);
    
    function renderSettings() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/profile/myProfile/settings.ejs", {
            req: req,
            data: req.data
        }, sendData);

        function sendData(err, str) {
            if (err) console.error(err);
            res.write(str);
            res.end();
        }
    }
}

// Post functions

function post(req, res) {
    if (req.route.length == 1) {
        redirect(res, "/");
        return;
    }

    if (req.session.username == req.route[1]) {
        if      (req.route.length == 2);
        else if (req.route[2] == "playingactivity");
        else if (req.route[2] == "videos");
        else if (req.route[2] == "pictures");
        else if (req.route[2] == "activity");
        else if (req.route[2] == "goals");
        else if (req.route[2] == "settings") processSettingsPost(req, res);
    } else conn.query("SELECT * FROM users where username = ?;", [req.route[1]], checkUser);

    function checkUser(err, rows, fields) {
        if (rows.length == 0) {
            error(req, res, "404");
            return;
        }

        req.data.user = rows[0];
        if      (req.route.length == 2);
        else if (req.route[2] == "playingactivity");
        else if (req.route[2] == "videos");
        else if (req.route[2] == "pictures");
        else if (req.route[2] == "activity");
        else if (req.route[2] == "goals");
    }
}

function processSettingsPost(req, res) {
    var errors = [];
    processPost(req, saveProfilePicture);

    function saveProfilePicture() {
        if (req.files.profilePicture != undefined) {
            if (req.files.profilePicture.size > 0) {
                var oldpath = req.files.profilePicture.path;
                var newpath = './public/images/profile/' + req.session.username + ".jpg";
                fs.rename(oldpath, newpath, function (err) {
                    if (err) throw err;
                    checkUserData();
                });
            } else checkUserData();
        } else checkUserData();
    }

    function checkUserData() {
        console.log(req.post);
        console.log(req.files);

        if (req.post.biography != "" && req.post.biography != undefined) {
            if (req.post.biography.length > 1000) errors.push("Please enter a biography 1000 characters or less");
        } else req.post.biography = null;

        if (req.post.height != "" && req.post.height != undefined) {
            if (isNaN(parseInt(req.post.height))) errors.push("Invalid height");
            else if (req.post.height > 250) errors.push("Invalid height");
        	else if (req.post.height < 50) errors.push("Invalid height");
        } else req.post.height = null;

        if (req.post.weight != "" && req.post.weight != undefined) {
            if (isNaN(parseInt(req.post.weight))) errors.push("Invalid weight");
            else if (req.post.weight > 200) errors.push("Invalid weight");
        	else if (req.post.weight < 20) errors.push("Invalid weight");
        } else req.post.weight = null;

        if (req.post.yearStartedPlaying != "" && req.post.yearStartedPlaying != undefined) {
        	if (isNaN(parseInt(req.post.yearStartedPlaying))) errors.push("Invalid year started playing");
            else if (req.post.yearStartedPlaying < 1900) errors.push("Invalid year started playing");
            else if (req.post.yearStartedPlaying > (new Date).getFullYear()) errors.push("Invalid year started playing");
        } else req.post.yearStartedPlaying = null;

        if (req.post.playingHand != "" && req.post.playingHand != undefined) {
        	if (req.post.playingHand != "right" && req.post.playingHand != "left") errors.push("Invalid playing hand");
        } else req.post.playingHand = null;

        if (req.post.backhandType != "" && req.post.backhandType != undefined) {
        	if (req.post.backhandType != "1" && req.post.backhandType != "2") errors.push("Invalid backhand type");
        } else req.post.backhandType = null;

        if (req.post.isLookingForCollegeScholarship != "" && req.post.isLookingForCollegeScholarship != undefined) {
            if (req.post.isLookingForCollegeScholarship != undefined &&
                req.post.isLookingForCollegeScholarship != 1 &&
                req.post.isLookingForCollegeScholarship != 0) errors.push("Invalid looking for scholarship status");
        } else req.post.isLookingForCollegeScholarship = null;

        if (req.post.isLookingForSponsorship != "" && req.post.isLookingForSponsorship != undefined) {
            if (req.post.isLookingForSponsorship != 1 &&
                req.post.isLookingForSponsorship != 0) errors.push("Invalid looking for sponsorship status");
        } else req.post.isLookingForSponsorship = null;

        if (req.post.internationalRanking != "" && req.post.internationalRanking != undefined) {
            if (isNaN(parseInt(req.post.internationalRanking))) errors.push("Invalid international ranking");
            else if (req.post.internationalRanking < 1  && req.post.internationalRanking != undefined) errors.push("Invalid international ranking");
            else if (req.post.internationalRanking > 9999) errors.push("Invalid international ranking");
        } else req.post.internationalRanking = null;

        if (req.post.internationalProfileLink != "" && req.post.internationalProfileLink != undefined) {
            if (req.post.internationalProfileLink.length > 128) errors.push("Please enter an international profile link 128 characters or less");
            else if (!req.post.internationalProfileLink.includes("www.atpworldtour.com/") && !req.post.internationalProfileLink.includes("www.wta.com/"))
                errors.push("Invalid international profile link");
        } else req.post.internationalProfileLink = null;

        if (req.post.itfProfileLink != "" && req.post.itfProfileLink != undefined) {
            if (req.post.itfProfileLink.length > 128) errors.push("Please enter an ITF profile link 128 characters or less");
            else if (!req.post.internationalProfileLink.includes("www.itftennis.com/procircuit/players/player/profile.aspx?playerid="))
                errors.push("Invalid ITF profile link");
        } else req.post.itfProfileLink = null;

        if (req.post.itfJuniorRanking != "" && req.post.itfJuniorRanking != undefined) {
            if (isNaN(parseInt(req.post.itfJuniorRanking))) errors.push("Invalid ITF junior ranking");
            else if (req.post.itfJuniorRanking < 1  && req.post.itfJuniorRanking != undefined) errors.push("Invalid ITF junior ranking");
            else if (req.post.itfJuniorRanking > 9999) errors.push("Invalid ITF junior ranking");
        } else req.post.itfJuniorRanking = null;

        if (req.post.itfJuniorProfileLink != "" && req.post.itfJuniorProfileLink != undefined) {
            if (req.post.itfJuniorProfileLink.length > 128) errors.push("Please enter an ITF junior profile link 128 characters or less");
            else if (!req.post.itfJuniorProfileLink.includes("www.itftennis.com/juniors/players/player/profile.aspx?playerid="))
                errors.push("Invalid ITF junior profile link");
        } else req.post.itfJuniorProfileLink = null;

        if (req.post.nationalRanking != "" && req.post.nationalRanking != undefined) {
            if (isNaN(parseInt(req.post.nationalRanking))) errors.push("Invalid national ranking, please enter a number greater than 0");
            else if (req.post.nationalRanking < 1)    errors.push("Please enter a national ranking greater than 0");
            else if (req.post.nationalRanking > 9999) errors.push("Please enter a national ranking under 10000 or contact us if your ranking is over 10000");
        } else req.post.nationalRanking = null;

        if (req.post.nationalProfileLink != "" && req.post.nationalProfileLink != undefined) {
            if (req.post.nationalProfileLink.length > 128) errors.push("Please enter an national profile link 128 characters or less");
            else if (!req.post.nationalProfileLink.includes("www.")) errors.push("Invalid national profile link");
        } else req.post.nationalProfileLink = null;

        if (req.post.mainStringTension != "" && req.post.mainStringTension != undefined) {
            if (isNaN(parseInt(req.post.mainStringTension))) errors.push("Invalid main string tension");
            else if (req.post.mainStringTension > 100) errors.push("Invalid main string tension");
            else if (req.post.mainStringTension < 0) errors.push("Invalid main string tension");
        } else req.post.mainStringTension = null;

        if (req.post.crossStringTension != "" && req.post.crossStringTension != undefined) {
            if (isNaN(parseInt(req.post.crossStringTension))) errors.push("Invalid main string tension");
            else if (req.post.crossStringTension > 100) errors.push("Invalid main string tension");
            else if (req.post.crossStringTension < 0) errors.push("Invalid main string tension");
        } else req.post.crossStringTension = null;

        if (req.post.country != "" && req.post.country != undefined) {
            var query = "SELECT countryID FROM countries WHERE name = ?";
            conn.query(query, req.post.country, checkCountryName);
        } else {
            req.post.country = null;
            queryRacquet();
        }
    }

    function checkCountryName(err, rows, fields) {
        if (!rows.length) errors.push("Invalid country");
        else req.post.country = rows[0].countryID;
        queryRacquet();
    }

    function queryRacquet() {
        if (req.post.racquetName != "" && req.post.racquetName != undefined && req.post.racquetModel != "" && req.post.racquetModel != undefined) {
            var query = "SELECT racquetID FROM racquets WHERE racquetName = ? AND model = ?";
            conn.query(query, [req.post.racquetName, req.post.racquetModel], checkRacquet);
        } else {
            req.post.racquetID = null;
            queryMainString();
        }
    }

    function checkRacquet(err, rows, fields) {
        console.log(rows);
        if (!rows.length) errors.push("Invalid racquet name or model");
        else req.post.racquetID = rows[0].racquetID;
        queryMainString();
    }

    function queryMainString() {
        if (req.post.mainStringName != "" && req.post.mainStringName != undefined && req.post.mainStringGuage != "" && req.post.mainStringGuage != undefined) {
            var query = "SELECT stringID FROM strings WHERE stringName = ? AND guage = ?";
            conn.query(query, [req.post.mainStringName, req.post.mainStringGuage], checkMainString);
        } else {
            req.post.mainStringID = null;
            queryCrossString();
        }
        
    }

    function checkMainString(err, rows, fields) {
        if (!rows.length) errors("Invalid main string");
        else req.post.mainStringID = rows[0].stringID;
        queryCrossString();
    }

    function queryCrossString() {
        if (req.post.crossStringName != "" && req.post.crossStringName != undefined && req.post.crossStringGuage != "" && req.post.crossStringGuage != undefined) {
            var query = "SELECT stringID FROM strings WHERE stringName = ? AND guage = ?";
            conn.query(query, [req.post.crossStringName, req.post.crossStringGuage], checkCrossString);
        } else {
            req.post.crossStringID = null;
            enterData();
        }
        
    }

    function checkCrossString(err, rows, fields) {
        if (!rows.length) errors("Invalid main string");
        else req.post.crossStringID = rows[0].stringID;
        enterData();
    }

    //No errors

    function enterData(err, hash) {
        if (errors.length > 0) returnErrors();
        else {
            var query = "UPDATE userData SET height = ?, weight = ?, countryID = ?, yearStartedPlaying = ?, playingHand = ?, backhandType = ?, isLookingForCollegeScholarship = ?, isLookingForSponsorship = ?, internationalRanking = ?, internationalProfileLink = ?, itfProfileLink = ?, itfJuniorRanking = ?, itfJuniorProfileLink = ?, nationalRanking = ?, nationalProfileLink = ?, racquetID = ?, mainStringID = ?, crossStringID = ?, mainStringTension = ?, crossStringTension = ?, biography = ? WHERE userID = ?";
            var values = [req.post.height, req.post.weight, req.post.country, req.post.yearStartedPlaying, req.post.playingHand, req.post.backhandType, req.post.isLookingForCollegeScholarship, req.post.isLookingForSponsorship, req.post.internationalRanking, req.post.internationalProfileLink, req.post.itfProfileLink, req.post.itfJuniorRanking, req.post.itfJuniorProfileLink, req.post.nationalRanking, req.post.nationalProfileLink, req.post.racquetID, req.post.mainStringID, req.post.crossStringID, req.post.mainStringTension, req.post.crossStringTension, req.post.biography, req.session.userID];
            console.log(conn.query(query, values, redirectToProfile).sql);
        }
    }

    function redirectToProfile(err) {
        if (err) console.error(err);
        res.writeHead(302,  {
            Location: "/profile/" + req.session.username,
        });

        res.end();

    }

    //Errors - Invalid registration

    function returnErrors() {
        console.log("Error in settings post");
        req.errors = errors;
        getUserDataSettings(req, renderSettings);
    }

    function renderSettings() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/profile/myProfile/settings.ejs", {
            req: req,
            data: req.data
        }, sendData);

        function sendData(err, str) {
            if (err) console.error(err);
            res.write(str);
            res.end();
        }
    }
}

module.exports = {
    get,
    post
};
