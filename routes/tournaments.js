var ejs  = require('ejs');
var error = require("./error");
var tournamentsFunctions = require("../functions/tournaments");

function get(req, res) {
	if      (req.route.length == 1) tournaments(req, res);
	else if (req.route.length == 2) {
		if      (req.route[1] == "atp")      ATPtournaments(req, res);
		else if (req.route[1] == "wta")      WTAtournaments(req, res);
		else if (req.route[1] == "national") nationalTournaments(req, res);
		else error(req, res, "404");
	} else if (req.route.length == 3) {
		if (req.route[1] == "national") {
			if (req.route[2] == "australia") australianTournaments(req, res);
			else error(req, res, "404");
		} else error(req, res, "404");
	} else if (req.route.length == 4) {
		if (req.route[1] == "national") {
			if (req.route[2] == "australia") {
				if      (req.route[3] == "amt") australianAMTtournaments(req, res);
				else if (req.route[3] == "ajt") australianAJTtournaments(req, res);
				else error(req, res, "404");
			} else error(req, res, "404");
		} else error(req, res, "404");
	} else error(req, res, "404");
}

function tournaments(req, res) {
	tournamentsFunctions.getAllTournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/tournaments.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function ATPtournaments(req, res) {
	tournamentsFunctions.getATPtournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/ATPtournaments.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function WTAtournaments(req, res) {
	tournamentsFunctions.getWTAtournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/WTAtournaments.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function nationalTournaments(req, res) {
	renderFile();

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/nationalTournaments.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function australianTournaments(req, res) {
	tournamentsFunctions.getAustralianTournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/australianTournaments.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function australianAMTtournaments(req, res) {
	tournamentsFunctions.getAustralianAMTtournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/australia/amt.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function australianAJTtournaments(req, res) {
	tournamentsFunctions.getAustralianAJTtournaments(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/tournaments/australia/ajt.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

module.exports = {
	get
}