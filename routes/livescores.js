var ejs  = require('ejs');

function get(req, res) {

    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/livescores/livescores.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });

}

module.exports = {
    get
};
