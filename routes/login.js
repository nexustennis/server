var ejs         = require("ejs");
var bcrypt      = require("bcrypt");
var conn        = require("../functions/database");
var processPost = require("../functions/processPost");
var redirect    = require("../functions/redirect");
var login       = require("../functions/login/login");

function get(req, res) {
    if (req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    res.writeHead(200, {
        "content-type": "text/html"
    });

    conn.query("SELECT * FROM users", queryResult);

    function queryResult(err, rows, fields) {
        if (err) console.log(err);

        ejs.renderFile("templates/login.ejs", {
            req: req,
            users: rows,
            errors: false
        }, renderResult);
    }

    function renderResult(err, str) {
        res.write(str);
        res.end();
    }

}

function post(req, res) {
    if (req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    var errors = [];

    processPost(req, getUserData);

    function getUserData() {

        if (req.post.email && req.post.password)
            conn.query("SELECT * FROM users where email = ?;", [req.post.email], comparePasswords);
        else {
            errors.push("Please enter email and password");
            returnErrors();
        }

    }

    function comparePasswords(err, rows, fields) {
        console.log(rows);
        if (err) {
            returnErrors();
        } else if (rows.length == 0) {
            errors.push("Error in Email or Password");
            returnErrors();
        } else {
            req.userData = {
                loggedIn:  true,
                userID:    rows[0].userID,
                username:  rows[0].username,
                firstname: rows[0].firstname,
                lastname:  rows[0].lastname,
                email:     rows[0].email
            }

            bcrypt.compare(req.post.password, rows[0].password, checkComparison);
        }
    }

    function checkComparison(err, result) {
        if (result) {
            setSession()
        } else {
            errors.push("Invalid login");
            returnErrors();
        }
    }

    function setSession() {
        login(req, redirectToHome);
    }

    function redirectToHome(err) {
        res.writeHead(302,  {
            Location: "/",
            "Set-Cookie": "session=" + req.uuid + "; Max-Age=2147483647;"
        });

        res.end();

    }

    function returnErrors() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/login.ejs", {
            errors: errors
        }, function(err, str) {
            if (err) {
                redirect(res, "/login");
            } else {
                res.write(str);
                res.end();
            }
        });
    }

}

module.exports = {
    get,
    post
};
