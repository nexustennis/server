var ejs  = require('ejs');
var error       = require('./error.js');
var getBrands = require("../functions/databases/getBrands");
var getRacquets = require("../functions/databases/racquets/getRacquets");
var getRacquet = require("../functions/databases/racquets/getRacquet");
var getOtherRacquetModels = require("../functions/databases/racquets/getOtherRacquetModels");
var getCustomisations = require("../functions/databases/customisations/getCustomisations");
var getRacquetCustomisations = require("../functions/databases/customisations/getRacquetCustomisations");

function get(req, res) {
	if (req.route.length == 1) databases(req, res);
	else if (req.route.length == 2) {
		if (req.route[1] == "racquets") racquets(req, res);
		else if (req.route[1] == "customisations") customisations(req, res);
		else if (req.route[1] == "strings") strings(req, res);
		else error(req, res, "404");
	} else if (req.route.length == 3) {
		if (req.route[1] == "racquets") {
			if (req.route[2] == "newracquet") newRacquet(req, res);
			else racquet(req, res);
		} else if (req.route[1] == "customisations") {
			if (req.route[2] == "newcustomisation") newCustomisation(req, res);
			else error(req, res, "404");
		} else error(req, res, "404");
	} else error(req, res, "404");
}

function databases(req, res) {
	renderFile();

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/databases.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function racquets(req, res) {
	getBrands(req, brandsResult);

	function brandsResult() {
		getRacquets(req, renderFile);
	} 

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/racquets/racquets.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function racquet(req, res) {
	getRacquet(req, checkRacquet);

	function checkRacquet() {
		if (req.data.racquet.length == 0) error(req, res, "404");
		else {
			req.data.racquet = req.data.racquet[0];
			getOtherRacquetModels(req, otherRacquetModelsResult);
		}
	}

	function otherRacquetModelsResult() {
		getRacquetCustomisations(req, renderFile);
	}

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/racquets/racquet.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function newRacquet(req, res) {
	renderFile();

	function renderFile() {
		console.log(req.data.racquet);

		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/racquets/newRacquet.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function customisations(req, res) {
	getCustomisations(req, renderFile);

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/customisations/customisations.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function newCustomisation(req, res) {
	getBrands(req, brandsResult);

	function brandsResult() {
		getRacquets(req, renderFile);
	} 

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/customisations/newCustomisation.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

function strings(req, res) {
	renderFile();

	function renderFile() {
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

	    ejs.renderFile("templates/databases/strings/strings.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) throw err;
	        res.write(str);
	        res.end();
	    });
	}
}

module.exports = {
	get
}