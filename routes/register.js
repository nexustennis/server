var botCheck    = require("../functions/botCheck");
var ejs         = require("ejs");
var bcrypt      = require("bcrypt");
var processPost = require("../functions/processPost");
var redirect    = require("../functions/redirect");
var genUUID     = require("../functions/genUUID");
var login       = require("../functions/login/login");
var isValidDate = require("../functions/register/isValidDate");
var conn        = require("../functions/database");

function get(req, res) {
    if (req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/register.ejs", {
        req: req,
        errors: false
    }, function(err, str) {
        if (err) {
            console.log(err);
            redirect(res, "/");
        } else {
            res.write(str);
            res.end();
        }
    });

}

function post(req, res) {
    if (req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    var errors = [];
    var validEmail = false;

    processPost(req, humanVerification);

    function humanVerification() {
        botCheck(req, errors, checkUserData);
    }

    function checkUserData() {
        console.log();
        if (typeof req.post.profileType == "string") req.post.profileType = [req.post.profileType];
        if (!req.post.profileType) errors.push("Please select a profile type");
        else if (req.post.profileType.length == 0) errors.push("Please select a profile type");
        else {
            for (i = 0; i < req.post.profileType.length; i++) {
                if      (req.post.profileType[i] == "player");
                else if (req.post.profileType[i] == "coach");
                else if (req.post.profileType[i] == "stringer");
                else if (req.post.profileType[i] == "customiser");
                else {
                    errors.push("Invalid profile type");
                    break;
                }
            }
        }

        if (!req.post.firstname) errors.push("Please enter a first name");
        else if (req.post.firstname.length > 64) errors.push("Please enter a first name 64 characters or less");
        else if (!(/^[a-zA-Z]+$/.test(req.post.firstname))) errors.push("Please enter a first name with only letters");

        if (!req.post.lastname) errors.push("Please enter a last name");
        else if (req.post.lastname.length > 64) errors.push("Please enter a last name 64 characters or less");
        else if (!(/^[a-zA-Z]+$/.test(req.post.lastname))) errors.push("Please enter a last name with only letters");

        if (!req.post.email) errors.push("Please enter an email");
        else if (req.post.email.length > 320) errors.push("Please enter an email address 320 characters or less");
        else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.post.email))) errors.push("Invalid email");
        else validEmail = true;

        if (!req.post.dateOfBirth) errors.push("Please enter a date of birth");
        else if (!isValidDate(req.post.dateOfBirth)) errors.push("Please enter a valid date of birth");

        if (!req.post.gender) errors.push("Please enter a gender");
        else if (req.post.gender != "male" && req.post.gender != "female") errors.push("Invalid gender");

        if (!req.post.password) errors.push("Please enter a password");
        else if (req.post.password.length <= 6) errors.push("Please enter a password longer than 6 characters");

        if (!req.post.confirmPassword) errors.push("Please enter a password confirmation");
        if (req.post.password != req.post.confirmPassword) errors.push("Please enter matching passwords");

        //queryDuplicateEmail();
        countUsersWithSameName();
    }

    function countUsersWithSameName() {
        //Check how many people with the same name
        var query = "SELECT count(*) FROM users WHERE firstname = ? AND lastname = ?";;
        conn.query(query, [req.post.firstname, req.post.lastname], generateUsername);
    }

    function generateUsername(err, rows, fields) {
        if (rows[0]["count(*)"] == 0) req.post.username = req.post.firstname.toLowerCase() + "." + req.post.lastname.toLowerCase();
        else req.post.username = req.post.firstname.toLowerCase() + "." + req.post.lastname.toLowerCase() + "." + rows[0]["count(*)"];

        queryDuplicateEmail();
    }

    function queryDuplicateEmail() {
        if (validEmail) {
            var query = "SELECT userID FROM users WHERE email = ?";
            conn.query(query, req.post.email, checkDuplicateEmail);
        } else hashPassword();
    }

    function checkDuplicateEmail(err, rows, fields) {
        if (!err) 
            if (rows.length > 0) errors.push("An acount with email " + req.post.email + " already exists");
        //else errors.append("An acount with email " + req.post.email + " already exists"); //UNKNOWN ERROR IF THIS OCCURS
        hashPassword();
    }

    function hashPassword() {
        if (errors.length == 0) bcrypt.hash(req.post.password, 10, registerUser);
        else returnErrors(errors);
    }

    //No errors

    function registerUser(err, hash) {

        var query = "INSERT INTO users (username, firstname, lastname, email, gender, dateOfBirth, password, profileType, permission) VALUES (?);";
        var values = [
            [req.post.username, req.post.firstname, req.post.lastname, req.post.email, req.post.gender, req.post.dateOfBirth, hash, JSON.stringify(req.post.profileType), "user"]
        ];
        conn.query(query, values, createUserDataEntry);
    }

    function createUserDataEntry(err, result) {
        console.log(result);
        if (err) {
            console.log(err);
            errors.push("Unknown Error");
            returnErrors(errors);
        } else {
            req.userData = {
                loggedIn:  true,
                userID:    result.insertId,
                username:  req.post.username,
                firstname: req.post.firstname,
                lastname:  req.post.lastname,
                email:     req.post.email
            }

            var query = "INSERT INTO userData (userID) VALUES (?);";
            conn.query(query, [result.insertId], setSession);
        }
    }

    function setSession(err, result) {
        if (err) console.error(err);
        login(req, redirectToHome);
    }

    function redirectToHome(err) {
        res.writeHead(302,  {
            Location: "/",
            "Set-Cookie": "session=" + req.uuid + "; Max-Age=2147483647;"
        });

        res.end();
    }

    //Errors - Invalid registration

    function returnErrors() {
        console.log("Error in register");
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/register.ejs", {
            req: req,
            errors: errors
        }, function(err, str) {
            if (err) {
                console.log(err);
                redirect(res, "/register");
            } else {
                res.write(str);
                res.end();
            }
        });
    }

}

module.exports = {
    get,
    post
};
