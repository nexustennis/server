var ejs  = require('ejs');
var error       = require('./error.js');

function get(req, res) {
    if (req.route.length == 1) onlineCoach(req, res);
    else if (req.route.length == 2) {
        if (req.route[1] == "packages") packages(req, res);
        else if (req.route[1] == "coaches") coaches(req, res);
        else error(req, res, "404");
    } else error(req, res, "404");
}

function onlineCoach(req, res) {
    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/onlinecoach/onlinecoach.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });
}

function packages(req, res) {
    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/onlinecoach/packages.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });
}

function coaches(req, res) {
    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/onlinecoach/coaches.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });
}

module.exports = {
    get
};
