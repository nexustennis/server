var ejs  = require('ejs');

function get(req, res) {

    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/index.ejs", {
        req: req,
        errors: null
    }, function(err, str) {
        if (err) throw err;//console.error(err);
        res.write(str);
        res.end();
    });

}

module.exports = {
    get
};
