var ejs                  = require('ejs');
var error                = require('./error.js');
var conn                 = require("../functions/database");
var redirect             = require("../functions/redirect");
var processPost          = require("../functions/processPost");
var isValidDate          = require("../functions/isValidDate");
var checkTourData        = require("../functions/tools/tourCalculator/checkTourData");
var filterTournaments    = require("../functions/tools/tourCalculator/filterTournaments");
var getTripTimes         = require("../functions/tools/tourCalculator/getTripTimes");
var tournamentsFunctions = require("../functions/tournaments");
var getUserData          = require("../functions/users/getUserData");


// Get functions

function get(req, res) {
	if      (req.route.length == 1) tools(req, res);
	else if (req.route.length == 2) {
		if      (req.route[1] == "tourbuilder")           tourBuilder(req, res);
		//else if (req.route[1] == "scoresandstatstracker") scoresAndStatsTracker(req, res);
		else if (req.route[1] == "tourcalculator")        tourCalculator(req, res);
		else error(req, res, "404");
	} else if (req.route.length == 3) {
		/*if (req.route[1] == "scoresandstatstracker") {
			if      (req.route[2] == "newmatch")  newMatch(req, res);
			else if (req.route[2] == "livematch") liveMatch(req, res);
			else error(req, res, "404");
		} else */
		error(req, res, "404");
	} else error(req, res, "404");
}

// main tools page

function tools(req, res) {
	res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/tools/tools.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });
}

// scores and stats tracker functions

function scoresAndStatsTracker(req, res) {
	res.writeHead(200, {
        'content-type': 'text/html'
    });

	ejs.renderFile("templates/tools/scoresAndStatsTracker/scoresAndStatsTracker.ejs", {
        req: req
    }, function(err, str) {
    	if (err) console.error(err);
        res.write(str);
        res.end();
    });
}

function newMatch(req, res) {
	res.writeHead(200, {
        'content-type': 'text/html'
    });

	ejs.renderFile("templates/tools/scoresAndStatsTracker/newMatch.ejs", {
        req: req
    }, function(err, str) {
    	if (err) console.error(err);
        res.write(str);
        res.end();
    });
}

function liveMatch(req, res) {
	res.writeHead(200, {
        'content-type': 'text/html'
    });

	ejs.renderFile("templates/tools/scoresAndStatsTracker/liveMatch.ejs", {
        req: req
    }, function(err, str) {
    	if (err) console.error(err);
        res.write(str);
        res.end();
    });
}

// tour builder functions

function tourBuilder(req, res) {
	tournamentsFunctions.getAllTournaments(req, checkLoginStatus);

	function checkLoginStatus() {
		if (req.session.loggedIn) {
			var query = "SELECT tournamentID FROM tours WHERE userID = ?";
			conn.query(query, req.session.userID, getCurrentTour);
		} else renderFile();
	}

	function getCurrentTour(err, result, fields) {
		req.data.currentTour = result;
		console.log(result);

		getUserData(req, renderFile);
	}

	function renderFile() {
		console.log(req.data.userData);
		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

		ejs.renderFile("templates/tools/tourBuilder/tourBuilder.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) console.error(err);
	        res.write(str);
	        res.end();
	    });
	}
}

// tour calculator functions

function tourCalculator(req, res) {
	if (req.route.length == 2) {
		queryCountry();

		function queryCountry() {
	        query = "SELECT * FROM countries";
	        conn.query(query, getCountries);
	    }

	    function getCountries(err, rows, fields) {
	        if (err) console.error(err);
	        req.data.countries = rows;
	        renderPage();
	    }

	    function renderPage() { 
			res.writeHead(200, {
		        'content-type': 'text/html'
		    });

	    	ejs.renderFile("templates/tools/tourCalculator/tourCalculator.ejs", {
		        req: req
		    }, function(err, str) {
		    	if (err) console.error(err);
		        res.write(str);
		        res.end();
		    });
		}
	} 

	else error(req, res, "404");
}

// Post functions

function post(req, res) {
	if (req.route.length == 1) {
		redirect(res, "/");
	} 

	//else if (req.route[1] == "scoresandstatstracker") scoresAndStatsTracker(req, res);
	else if (req.route[1] == "tourcalculator")        processTourCalculatorPost(req, res);
	else if (req.route[1] == "tourbuilder")           processTourBuilderPost(req, res);
	
	else error(req, res, "404");
}

function processTourBuilderPost(req, res) {
	if (!req.session.loggedIn) {
        redirect(res, "/");
        return;
    }

    var tournaments;
    var queryList = [];

	processPost(req, checkData);

	function checkData() {
		tournaments = JSON.parse(req.post.tournaments);
		
		//check if tournament is in tour

		//check tournamentID exists with the same userID

		var query = "SELECT * FROM tours WHERE userID = ?;";
		conn.query(query, req.session.userID, getCurrentTour);
	}

	function getCurrentTour(err, result, fields) {
		if (err) console.log(err);

		var currentTour = result;
		var existingTournaments = [];


		for (var i = 0; i < tournaments.length; i++) {
			var tournamentExistsInCurrentTour = false;

			for (var j = 0; j < currentTour.length; j++) {
				if (currentTour[j].tournamentID == tournaments[i]) {
					tournamentExistsInCurrentTour = true;
					existingTournaments.push(currentTour[j].tournamentID);
					currentTour.splice(j, 1);
					break;
				}
			}

			if (!tournamentExistsInCurrentTour) {
				var entry = [req.session.userID, tournaments[i]];
				queryList.push(entry);
			}
		}

		var deletedTournaments = currentTour.filter(tournament => !existingTournaments.includes(tournament.tournamentID));

		var deletedQueryList = [];

		for (var i = 0; i < deletedTournaments.length; i++) {
			deletedQueryList.push(deletedTournaments[i].tourID);
		}

		if (deletedQueryList.length == 0 && queryList.length == 0) {
			finish();
			return;
		} else if (deletedQueryList.length == 0) {
			var query = "INSERT INTO tours (userID, tournamentID) VALUES ?";
			var data  = [queryList];
		} else if (queryList.length == 0) {
			var query = "DELETE FROM tours WHERE tourID IN (?)";
			var data  = [deletedQueryList];
		} else {
			var query = "DELETE FROM tours WHERE tourID IN (?); INSERT INTO tours (userID, tournamentID) VALUES ?;";
			var data  = [deletedQueryList, queryList];
		}
		
		conn.query(query, data, finish);
	}

	function finish(err) {
		if (err) console.log(err);
		redirect(res, "/tools/tourbuilder");
	}
}

function processTourCalculatorPost(req, res) {
	var errors = [];
	processPost(req, checkData);

	function checkData() {
		checkTourData(req, errors, queryTournaments);
	}

	function queryTournaments() {
		realquery = "SELECT * FROM tournaments WHERE startDate >= ? AND endDate <= ?";
		query = "SELECT * FROM tournaments WHERE startDate >= ? AND endDate <= ?";
		if (req.post.manualTournamentLevel.length > 0) query += " AND (";
		for (var i = 0; i < req.post.manualTournamentLevel.length; i++) {
			query += "level = '" + req.post.manualTournamentLevel[i] + "'";
			if (req.post.manualTournamentLevel.length - i != 1) query += " OR ";
		}
		query += ");";
		console.log(query);
		console.log(conn.query(query, [req.post.startDate, req.post.endDate], queryTournamentsResult).sql);
	}

	function queryTournamentsResult(err, rows, fields) {
		req.data.unfilteredTournaments = rows;

		filterTournaments(req, calculateTour);
	}

	function calculateTour() {
		getTripTimes(req, renderFile);
		//if (errors) console.log(errors);
		//renderFile();
	}

	function renderFile() {

		req.data.result = req.data.tournaments;

		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

    	ejs.renderFile("templates/tools/tourCalculator/result.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) console.error(err);
	        res.write(str);
	        res.end();
	    });		
	}

	function returnErrors() {
		req.data.errors = errors;

		res.writeHead(200, {
	        'content-type': 'text/html'
	    });

    	ejs.renderFile("templates/tools/tourCalculator/tourCalculator.ejs", {
	        req: req
	    }, function(err, str) {
	    	if (err) console.error(err);
	        res.write(str);
	        res.end();
	    });
	}
}

module.exports = {
    get,
    post
};
