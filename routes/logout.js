var ejs      = require('ejs');
var fs       = require("fs");
var conn     = require("../functions/database");
var redirect = require("../functions/redirect");

function get(req, res) {
    if (req.session.loggedIn) {
        conn.query("DELETE FROM sessions WHERE ?", {
            uuid: req.cookies.session
        }, redirectToHome);
    } else {
        redirect(res, "/");
    }

    function redirectToHome() {
        res.writeHead(302,  {
            Location: "/",
            "Set-Cookie": "session=deleted; expires=Thu, 01 Jan 1970 00:00:00 GMT;"
        });

        res.end();
    }
}

module.exports = {
    get
};
