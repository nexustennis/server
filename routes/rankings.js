var ejs  = require('ejs');
var getAllTopTenLatestRankings = require("../functions/rankings/getAllTopTenLatestRankings");
var getATPsinglesRankings = require("../functions/rankings/atp/getATPsinglesRankings");
var error               = require('./error.js');

function get(req, res) {
    if (req.route.length == 1) rankings(req, res);
    else if (req.route.length == 2) {
        if (req.route[1] == "atp") atpRankings(req, res);
        else if (req.route[1] == "wta") wtaRankings(req, res);
        else if (req.route[1] == "itf") itfRankings(req, res);
        else error(req, res, "404");
    } else if (req.route.length == 3) {
        if (req.route[1] == "atp") {
            if (req.route[2] == "singles") atpSinglesRankings(req, res);
            else error(req, res, "404");
        } else error(req, res, "404");
    } else error(req, res, "404");
}

function rankings(req, res) {
    getAllTopTenLatestRankings(req, renderPage);

    function renderPage() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/rankings/rankings.ejs", {
            req: req
        }, function(err, str) {
            if (err) throw err;
            res.write(str);
            res.end();
        });
    }
}

function atpRankings(req, res) {
    getAllTopTenLatestRankings(req, renderPage);

    function renderPage() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/rankings/atp/atpRankings.ejs", {
            req: req
        }, function(err, str) {
            if (err) throw err;
            res.write(str);
            res.end();
        });
    }
}

function atpSinglesRankings(req, res) {
    getATPsinglesRankings(req, renderPage);

    function renderPage() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/rankings/atp/atpSinglesRankings.ejs", {
            req: req
        }, function(err, str) {
            if (err) throw err;
            res.write(str);
            res.end();
        });
    }
}

module.exports = {
    get
};
