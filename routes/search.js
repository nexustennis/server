var ejs  = require('ejs');
var conn = require("../functions/database");
var processPost = require("../functions/processPost");
var error       = require('./error.js');

function get(req, res) {

    res.writeHead(200, {
        'content-type': 'text/html'
    });

    ejs.renderFile("templates/search/search.ejs", {
        req: req
    }, function(err, str) {
        res.write(str);
        res.end();
    });

}

function post(req, res) {
    req.searchResult = {};

    processPost(req, searchUsers);

    function searchUsers() {
        if (req.post.search == undefined) {
            error(req, res, "404");
            return;
        }
        var query = "SELECT username, firstname, lastname, profileType FROM users WHERE Match(username, firstname, lastname) Against(?);";
        conn.query(query, [req.post.search], processUserSearch);
    }

    function processUserSearch(error, rows, fields) {
        if (error) console.error(error);
        console.log(rows);
        req.searchResult.users = rows;
        renderSearchResults();
    }

    function renderSearchResults() {
        res.writeHead(200, {
            'content-type': 'text/html'
        });

        ejs.renderFile("templates/search/results.ejs", {
            req: req
        }, function(err, str) {
            if (err) console.error(err);
            res.write(str);
            res.end();
        });
    }

}

module.exports = {
    get,
    post
};
